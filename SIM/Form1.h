#pragma once

#include <limits>
#include <string>
#include <msclr/marshal.h>
#include <msclr/marshal_cppstd.h>
#include "dcmtk/dcmdata/dctk.h"
#include "dcmtk/dcmimgle/dcmimage.h"

using namespace System::IO;
using namespace msclr::interop;

namespace SIM {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	protected: 
	private: System::Windows::Forms::OpenFileDialog^  openFileDialog1;
	private: System::Windows::Forms::Button^  loadDicomButton;


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->openFileDialog1 = (gcnew System::Windows::Forms::OpenFileDialog());
			this->loadDicomButton = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Location = System::Drawing::Point(12, 12);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(128, 128);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// openFileDialog1
			// 
			this->openFileDialog1->FileName = L"openFileDialog1";
			this->openFileDialog1->FileOk += gcnew System::ComponentModel::CancelEventHandler(this, &Form1::openFileDialog1_FileOk);
			// 
			// loadDicomButton
			// 
			this->loadDicomButton->Location = System::Drawing::Point(12, 146);
			this->loadDicomButton->Name = L"loadDicomButton";
			this->loadDicomButton->Size = System::Drawing::Size(246, 23);
			this->loadDicomButton->TabIndex = 1;
			this->loadDicomButton->Text = L"Za�aduj DICOM";
			this->loadDicomButton->UseVisualStyleBackColor = true;
			this->loadDicomButton->Click += gcnew System::EventHandler(this, &Form1::loadDicomButton_Click);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(294, 175);
			this->Controls->Add(this->loadDicomButton);
			this->Controls->Add(this->pictureBox1);
			this->Name = L"Form1";
			this->Text = L"Form1";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void openFileDialog1_FileOk(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
			 }
	private: System::Void loadDicomButton_Click(System::Object^  sender, System::EventArgs^  e) {
				  OpenFileDialog^ openFileDialog1 = gcnew OpenFileDialog;

				  openFileDialog1->InitialDirectory = "c:\\";
				  openFileDialog1->Filter = "Pliki DICOM (*.dcm) | *.dcm";
				  openFileDialog1->FilterIndex = 2;
				  openFileDialog1->RestoreDirectory = true;
				  if ( openFileDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK )
				  {
					String^ strFileName = openFileDialog1->InitialDirectory + openFileDialog1->FileName;
					std::string nativeStringFileName(marshal_as<std::string>(strFileName));
					const char * charFileName = nativeStringFileName.c_str();
					DicomImage *image = new DicomImage(charFileName);
					if (image != NULL)
					{
						if (image->getStatus() == EIS_Normal)
						{
							Uint8 *pixelData = (Uint8 *)(image->getOutputData(8 /* bits per sample */));
							if (pixelData != NULL)
							{
								Bitmap^ image = gcnew Bitmap(128, 128, 8, System::Drawing::Imaging::PixelFormat::Canonical, IntPtr(pixelData));
								pictureBox1->Image = dynamic_cast<Image^>(image);
							}
						}			
					}
				  }
			 }
	};
}

